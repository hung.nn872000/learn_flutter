import 'dart:math';

import 'package:flutter/material.dart';


void main() {
  runApp(const MyApp());
}

final color = [Colors.white, Colors.blue, Colors.green, Colors. yellow, Colors.red, Colors.black, Colors.purple, Colors.black26, Colors.lightGreen];

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;

  void _incrementCounter() {
    setState(() {
      _counter++;
    });
  }

  bool isOdd() =>  _counter % 2 != 0;
  final random = Random();
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: isOdd()? color[0]: color[random.nextInt(color.length)],
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child:  (
            RawMaterialButton(
              onPressed: _incrementCounter,
              elevation: 2.0,
              fillColor: Colors.brown,
              child: Icon(
                Icons.add,
                size: 35.0,
              ),
              padding: EdgeInsets.all(15.0),
              shape: CircleBorder(),
            )
        ),
        ),
      bottomSheet: Container(
        child: Text('The times you push the button: $_counter', style: TextStyle(color: Colors.blue, fontSize: 22)),
      ),
    );

  }
}
